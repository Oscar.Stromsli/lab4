package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;


public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else if (random.nextBoolean()){
                    currentGeneration.set(row, col, CellState.DEAD);
                }else{
                    currentGeneration.set(row,col,CellState.DYING);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int r = 0; r < nextGeneration.numRows(); r++) {
            for (int c = 0; c < nextGeneration.numColumns(); c++) {
                currentGeneration.set(r, c, getNextCell(r, c));

            }
        }
    }


        @Override
    public CellState getNextCell(int row, int col) {
            CellState currentCellstate = currentGeneration.get(row, col);
            int count = countNeighbors(row, col, CellState.ALIVE);
            if (getCellState(row, col) == CellState.ALIVE) {
                return CellState.DYING;
            }
            else if(getCellState(row,col) == CellState.DYING){
                return CellState.DEAD;
            }
            else if(getCellState(row, col)== CellState.DEAD){
                if (count == 2){
                    return CellState.ALIVE;
                }
                else{
                    return CellState.DEAD;
                }
            }
            return currentCellstate;
        }

    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;

        for (int r = row-1; r <= row+1; r++){
            for (int c = col-1; c<=col+1; c++){
                if(c >= numberOfColumns()){
                    continue;
                }
                else if(c < 0){
                    continue;
                }
                else if(r >= numberOfRows()){
                    continue;
                }
                else if(r < 0){
                    continue;
                }
                else if(c==col && r==row){
                    continue;
                }
                if (currentGeneration.get(r, c) == state){
                    count++;
                }
            }
        }
        return count;
    }


    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
