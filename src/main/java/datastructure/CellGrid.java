package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private final int rows;
    private final int columns;
    private final CellState [][] board;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        board = new CellState[rows][columns];
        for (int r = 0; r < numRows(); r++) {
            for (int c = 0; c < numColumns(); c++) {
                board [r][c] = initialState;
            }
        }
	}

    public boolean checkIndexValue(int row, int column) {
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (checkIndexValue(row, column)) {
            board[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }


    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (checkIndexValue(row, column)) {
            return board[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }


    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid grid = new CellGrid(this.rows, this.columns, null);
        for(int r = 0; r < this.numRows(); r++){
            for(int c = 0; c < this.numColumns(); c++){
                grid.set(r, c, get(r, c));
            }
        }
        return grid;

    }
    
}
